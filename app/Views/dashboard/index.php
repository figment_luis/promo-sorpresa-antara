<?= $this->extend('layout/app') ?>

<!-- Titulo de documento -->
<?= $this->section('title') ?>Panel de Control<?= $this->endSection() ?>

<!-- Contenido principal -->
<?= $this->section('content') ?>
<div class="container container-flex__column">
        <ul class="info-user" style="width: 450px; margin-bottom: 2rem;">
                <li style="margin-bottom: .5rem; font-weight: 700;">Hola: <?= $admin_fullname ?></li>
                <li>Bienvenido al panel de control administrativo del sitema Antara</li>
                <li>¿Que vamos hacer hoy?</li>
            </ul>
        <h5 class="main__title">Panel de Control Administrativo</h5>
        <ul class="main__list">
            <li class="main__item">
                <a href="<?= base_url('users') ?>" class="main__link">
                    <i class="fas fa-user main__icon"></i> Registrar nuevos clientes
                </a>
            </li>
            <li class="main__item">
                <a href="<?= base_url('search') ?>" class="main__link">
                    <i class="fas fa-ticket-alt main__icon"></i> Registrar tickets
                </a>
            </li>
            <li class="main__item">
                <a href="<?= base_url('report') ?>" class="main__link">
                    <i class="fas fa-file-csv main__icon"></i> Generar reporte
                </a>
            </li>
        </ul>
</div>
<?= $this->endSection() ?>
