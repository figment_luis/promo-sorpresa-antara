<?= $this->extend('layout/app') ?>

<!-- Titulo de documento -->
<?= $this->section('title') ?>Términos y Condiciones<?= $this->endSection() ?>

<!-- Contenido principal -->
<?= $this->section('content') ?>
<div class="container">
    <div class="row">
        <div class="col-12 terms">
            <h5 class="terms__title">Términos y Condiciones (certificado Antara) (la “Promoción”)</h5>
            <p class="terms__clause"><strong>Responsable de la promoción:</strong> <span class="terms__highlight">ANTARA FASHION HALL, S.A.</span> Av. Ejército Nacional 843, Zona Polanco, Granada, Miguel Hidalgo, 11520 Ciudad de México, CDMX.</p>
            <p class="terms__clause"><strong>Incentivo de la Promoción:</strong> 1,000 certificados de regalo con valor de $1,000 (mil pesos MN) cada uno, canjeables en los establecimientos participantes, exclusivamente en <span class="terms__highlight">ANTARA FASHION HALL.</span></p>
            <p class="terms__clause"><strong>Vigencia de la Promoción:</strong> Del 14 de diciembre de 2020 al 23 de diciembre de 2020 o hasta agotar existencias.</p>
            <p class="terms__clause"><strong>Mecánica de la Promoción:</strong> Por cada $5,000 (cinco mil pesos MXN) de compra en las marcas participantes dentro de Antara llévate un certificado de regalo de tu elección con valor de $1,000 (mil pesos MN) para gastar en las boutiques de Antara. Consulta la vigencia del certificado.</p>
            <h5 class="terms__subtitle">Bases de la participación:</h5>
            <ul class="bases">
                <li class="bases__item">Promoción abierta al público en general.</li>
                <li class="bases__item">La Promoción es exclusivamente para personas mayores de 18 años y estará limitada a los premios designados para la promoción.</li>
                <li class="bases__item">El monto de compra de $5,000 (cinco mil pesos MN) puede acreditarse en un solo ticket o con tickets acumulables de las marcas participantes.</li>
                <li class="bases__item">Sólo podrán canjearse los certificados el mismo día de la compra.</li>
                <li class="bases__item">No se aceptarán tickets con fecha diferente del día actual.</li>
                <li class="bases__item">Máximo se entregarán 3 (tres) certificados de regalo por persona por día.</li>
                <li class="bases__item">No podrán participar los empleados de las empresas del mismo grupo de intereses corporativos de <span class="terms__highlight">ANTARA FASHION HALL</span>, y las agencias de publicidad o de servicios que mantengan una relación con las sociedades mercantiles antes referidas, y sus familiares en primer grado (padres, hermanos, abuelos e hijos).</li>
            </ul>
            <p class="terms__clause"><strong>Establecimientos para aplicar la Promoción:</strong> los tickets de compra deben registrarse el mismo día de la compra en el módulo de Concierge ubicado en <span class="terms__highlight">ANTARA FASHION HALL.</span></p>
            <p class="terms__clause">Sólo aplica en marcas participantes. </p>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
