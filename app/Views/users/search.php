<?= $this->extend('layout/app') ?>

<!-- Titulo de documento -->
<?= $this->section('title') ?>Buscador de Usuarios<?= $this->endSection() ?>

<!-- Contenido principal -->
<?= $this->section('content') ?>
<div class="container container-search">
    <h5 class="main__title">Buscador de Clientes en el Sistema</h5>
     <a href="<?= base_url('dashboard') ?>" class="main__link" style="font-size: .9rem; margin-bottom: 2.5rem; margin-top: -1rem">
        <i class="fas fa-tachometer-alt main__icon"></i> Regresar al Dashboard
    </a>
    <?php if(session('message')): ?>
        <div class="alert alert-danger form-message">
            <i class="fas fa-volume-up"></i> <?= session('message') ?>
        </div>
    <?php endif; ?>
    <form action="<?= base_url('/search') ?>" method="POST" class="form-search">
        <?= csrf_field() ?>
        <div class="form-group row">
            <div class="col-sm-9">
                <input type="search" name="query"  class="form-control <?= session('errors.query') ? 'is-invalid' : '' ?>" placeholder="Buscar cliente por..." id="query" value="<?= old('query') ?>" autofocus required>
                <div class="invalid-feedback">
                    <?= session('errors.query') ?>
                </div>
            </div>
            <div class="col-sm-3">
                <button type="submit" name="send" class="btn btn-block btn-add">Buscar</button>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-8">
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="email" name="field" class="custom-control-input" value="email" checked>
                    <label class="custom-control-label" for="email">Correo Electrónico</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="phone" name="field" class="custom-control-input" value="phone">
                    <label class="custom-control-label" for="phone">Teléfono</label>
                </div>
            </div>
            <div class="col-sm-4"></div>
        </div>
    </form>

</div>
<?= $this->endSection() ?>
