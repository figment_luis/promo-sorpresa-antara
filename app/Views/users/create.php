<?= $this->extend('layout/app') ?>

<!-- Titulo de documento -->
<?= $this->section('title') ?>Registro de Usuarios<?= $this->endSection() ?>

<!-- Contenido principal -->
<?= $this->section('content') ?>
<div class="container container-register">
    <div>
        <h5 class="main__title">Registro de Clientes</h5>
        <a href="<?= base_url('dashboard') ?>" class="main__link" style="font-size: .9rem;">
            <i class="fas fa-tachometer-alt main__icon"></i> Regresar al Dashboard
        </a>
    </div>
    <form action="<?= base_url('/users') ?>" method="POST" class="form-register">
        <?= csrf_field() ?>
        <div class="form-group">
            <label for="first_name">Nombre(s)</label>
            <input type="text" name="first_name" class="form-control <?= session('errors.first_name') ? 'is-invalid' : '' ?>" id="first_name" placeholder="Ingrese el nombre del cliente" value="<?= old('first_name') ?>"  required>
            <div class="invalid-feedback"><?= session('errors.first_name') ?></div>
        </div>
        <div class="form-group">
            <label for="last_name">Apellidos</label>
            <input type="text" name="last_name" class="form-control <?= session('errors.last_name') ? 'is-invalid' : '' ?>" id="last_name" placeholder="Ingrese los apellidos del cliente" value="<?= old('last_name') ?>"  required>
            <div class="invalid-feedback"><?= session('errors.last_name') ?></div>
        </div>
        <div class="form-group">
            <label for="email">Correo Electrónico</label>
            <input type="email" name="email" class="form-control <?= session('errors.email') ? 'is-invalid' : '' ?>" id="email" placeholder="Ingrese la dirección de correo electrónico del cliente" value="<?= old('email') ?>"  required>
            <div class="invalid-feedback"><?= session('errors.email') ?></div>
        </div>
        <div class="form-group">
            <label for="phone">Número Telefónico</label>
            <input type="tel" name="phone" class="form-control <?= session('errors.phone') ? 'is-invalid' : '' ?>" id="phone" placeholder="Formato de (10 dígitos) - 713000000" value="<?= old('phone') ?>" required maxlength="10" minlength="10" pattern="[0-9]{10}">
            <div class="invalid-feedback"><?= session('errors.phone') ?></div>
        </div>
        <div class="form-group">
            <button type="submit" name="send" class="btn btn-secondary btn-add float-right">Registrar</button>
        </div>
    </form>
</div>

<?= $this->endSection() ?>
