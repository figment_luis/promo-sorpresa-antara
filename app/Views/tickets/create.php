<?= $this->extend('layout/app') ?>

<!-- Titulo de documento -->
<?= $this->section('title') ?>Registro de Tickets<?= $this->endSection() ?>

<!-- Contenido principal -->
<?= $this->section('content') ?>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h5 class="main__title">Registrar Tickets de Compra</h5>
            <ul class="info-user">
                <li><strong>Nombre del Cliente:</strong> <?= $client_fullname ?></li>
                <li><strong>Certificados Registrados al Día:</strong> <?= $current_redemptions ?> de <?= $redemptions_per_day ?></li>
            </ul>
            <form id="form-tickets">
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <label for="number">Número de Ticket</label>
                        <input type="text" name="number" class="form-control" id="number" placeholder="Ingrese el número de ticket de compra" required autofocus>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="amount">Monto de Compra</label>
                        <input type="text" name="amount" class="form-control" id="amount" placeholder="Ej. 1273.50" required pattern="[0-9]+([\.][0-9]+)?">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <label for="date">Fecha y Hora de Compra</label>
                        <input type="datetime-local" name="date" class="form-control" id="date" step="1" required>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="store">Tienda</label>
                        <select name="store" required class="custom-select" id="store">
                            <option value="" selected disabled>Seleccione una tienda</option>
                            <?php foreach ($stores as $store): ?>
                                <option value="<?= $store['id'] ?>"><?= $store['name'] ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 mb-3">
                        <button class="btn btn-secondary btn-block btn-add" name="add" id="btn-add" type="submit">Agregar Ticket</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-6">
            <div class="title__container">
                <h5 class="main__title" style="margin-bottom: 0;">Tickets Registrados</h5>
                <form action="<?= base_url('cancel-transaction') ?>" method="post" id="form-cancel-transaction">
                    <button type="submit" class="btn-danger btn btn-sm" title="Permite cancelar la operación actual de canje." name="send" id="btn-cancel"><i class="far fa-stop-circle" style="margin-right: .2rem;"></i> Cancelar Operación</button>
                </form>
            </div>
            <table class="table table-striped table-bordered table-sm table-tickets">
                <thead class="thead-dark text-center">
                    <tr>
                        <th>Ticket</th>
                        <th>Monto</th>
                        <th>Tienda</th>
                        <th>Fecha y Hora</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody id="table-tickets">
                    <?php foreach ($tickets as $ticket): ?>
                        <tr>
                            <td><?= $ticket['number'] ?></td>
                            <td class="text-right">$ <?= number_format($ticket['amount'], 2) ?></td>
                            <td title="<?= $ticket['store'] ?>"><?= substr($ticket['store'], 0, 15) ?></td>
                            <td class="text-center"><?= $ticket['date'] ?></td>
                            <td class="text-center">
                                <a href="#" class="btn-delete" data-id="<?= $ticket['id'] ?>" title="Eliminar">
                                    <i class="fas fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4" class="text-right"><i class="fas fa-dollar-sign info-highlight"></i> <strong>Monto Acumulado:</strong></td>
                        <td id="total_amount" class="text-right">$<?= number_format($total_amount, 2) ?></td>
                    </tr>
                </tfoot>
            </table>
            <a href="<?= base_url('redemptions') ?>" class="btn float-right btn-sm btn-next"
                id="btn-next"
                style="display: none;">Siguiente</a>
        </div>
    </div>



    <div class="row">
        <div class="col-md-10">

        </div>
        <div class="col-md-2">

        </div>
    </div>
</div>
<?= $this->endSection() ?>

<!-- Sección de de Hojas de estilo -->
<?= $this->section('scripts') ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<?= $this->endSection() ?>

<!-- Sección de scripts -->
<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/libs/jquery.number.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script>
    $(document).ready(function() {
        let baseUrl = '<?= base_url() ?>'

        // Verificar si el usuario puede canjear sus tickets
        let total_amount = '<?= $total_amount ?>';
        if (total_amount >= 5000) {
            $('#btn-next').show()
        }

        $('#form-tickets').submit(function(e) {
            e.preventDefault();
            // Deshabilitar el boton de agregar tickets
            $("#btn-add").prop('disabled', true)
            $.ajax({
                url: baseUrl + '/tickets',
                method: 'post',
                data: $(this).serialize(),
                headers: {'X-Requested-With': 'XMLHttpRequest'},
                dataType: 'json',
            })
            .done(function(data, textStatus, jqXHR) {
                console.log(data)
                if (data.status === 'success') {
                    toastr.success(data.message, 'Información del sistema', {timeOut: 1500})
                    // Limpiar campos de Formulario
                    $('#number, #amount, #date, #store').val('')
                    $('#number').focus()
                    // Llenar tabla con los nuevos tickets registrados en el sistema
                    $('#table-tickets tr').remove();
                    $.each(data.tickets, function(index, value) {
                        $('#table-tickets').append(`<tr><td>${value.number}</td><td class="text-right">$ ${$.number(value.amount, 2, '.', ',')}</td><td title="${value.store}">${value.store.substring(0, 15)}</td><td class="text-center">${value.date}</td><td class="text-center"><a href="#" class="btn-delete" data-id="${value.id}" title="Eliminar"><i class="fas fa-trash"></i></a></td></tr>`)
                    })
                    $('#total_amount').html(`$${$.number(data.total_amount, 2, '.', ',')}`)
                    // Verificar si el usuario puede canjear sus tickets
                    if (data.total_amount >= 5000) {
                        $('#btn-next').show()
                    }
                } else {
                    // El ticket ingresado ya se encuentra previemente registrado o la fecha es diferente al día de hoy
                    Swal.fire({
                        html: data.message,
                        icon: 'error',
                        title: 'Lo sentimos',
                        confirmButtonText: 'Aceptar',
                        confirmButtonColor: '#158f97'
                        //text: data.message,
                    })
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus)
                console.log(jqXHR.responseText)
            })
            .always(function() {
                // Habilitar nuevamente el boton de agregat tickets
                $("#btn-add").prop('disabled', false)
            });
        })

        $('body').delegate('.btn-delete', 'click', function(e) {
            e.preventDefault();
            $.ajax({
                url: baseUrl + '/tickets/' + $(this).data('id'),
                method: 'post',
                headers: {'X-Requested-With': 'XMLHttpRequest'},
                dataType: 'json',
            })
            .done(function(data, textStatus, jqXHR) {
                console.log(data)
                 if (data.status === 'success') {
                    toastr.info(data.message, 'Información del sistema', {timeOut: 1500})
                    $('#table-tickets tr').remove();
                    $.each(data.tickets, function(index, value) {
                        $('#table-tickets').append(`<tr><td>${value.number}</td><td class="text-right">$ ${$.number(value.amount, 2, '.', ',')}</td><td title="${value.store}">${value.store.substring(0, 15)}</td><td class="text-center">${value.date}</td><td class="text-center"><a href="#" class="btn-delete" data-id="${value.id}" title="Eliminar"><i class="fas fa-trash"></i></a></td></tr>`)
                    })
                    $('#total_amount').html(`$${$.number(data.total_amount, 2, '.', ',')}`)
                    // Verificar si el usuario puede canjear sus tickets
                    if (data.total_amount < 5000) {
                        $('#btn-next').hide()
                    }
                } else {
                    // El ticket seleccionado no fue econtrado en el sistema para eliminarlo
                    Swal.fire({
                        icon: 'error',
                        title: 'Lo sentimos',
                        text: data.message,
                        confirmButtonText: 'Aceptar',
                        confirmButtonColor: '#158f97'
                    })
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus)
                console.log(jqXHR.responseText)
            })
            console.log($(this).data('id'))
        })

        $('#form-cancel-transaction').submit(function(e) {
            e.preventDefault();
            e.stopPropagation();
            // Deshabilitar el botón cancelar operación
            $('#btn-cancel').prop('disabled', true)
            Swal.fire({
                title: 'Alerta',
                text: "¿Realmente desea cancelar la operación?",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                showCancelButton: true,
                confirmButtonColor: '#158f97',
                cancelButtonColor: '#dc3545',
                confirmButtonText: 'Si, proceder a cancelar',
                cancelButtonText: 'No',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    // Solicitar al servidor que cancele la operación
                    $.ajax({
                        url: baseUrl + '/cancel-transaction',
                        method: 'post',
                        headers: {'X-Requested-With': 'XMLHttpRequest'},
                        dataType: 'json',
                    })
                    .done(function(data, textStatus, jqXHR) {
                        console.log(data)
                        if (data.status === 'success') {
                            // Confirmar al usuario y redireccionar al Dashboard
                            Swal.fire({
                                icon: 'success',
                                title: 'Felicidades',
                                text: data.message,
                                allowOutsideClick: false,
                                allowEscapeKey: false,
                                confirmButtonText: 'Aceptar',
                                confirmButtonColor: '#158f97'
                            }).then(function() {
                                // Redireccionar al Concierge al Dashboard Administrativo.
                                //window.location.replace(baseUrl);
                                window.location = baseUrl + '/dashboard';
                            })

                        } else {
                            // Fue imposible cancelar la operación
                            Swal.fire({
                                icon: 'error',
                                title: 'Lo sentimos',
                                text: data.message,
                                confirmButtonColor: '#158f97',
                                confirmButtonText: 'Aceptar'
                            })
                        }
                    })
                    .fail(function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus)
                        console.log(jqXHR.responseText)
                    })
                    .always(function() {
                        // Habilitar nuevamente el botón canjear
                        $('#btn-cancel').prop('disabled', false)
                    })
                } else if ( result.dismiss === Swal.DismissReason.cancel) {
                    $('#btn-cancel').prop('disabled', false)
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
