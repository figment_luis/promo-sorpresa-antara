<?= $this->extend('layout/app') ?>

<!-- Titulo de documento -->
<?= $this->section('title') ?>Login<?= $this->endSection() ?>

<!-- Contenido principal -->
<?= $this->section('content') ?>
<div class="container container-search">
    <div>
        <h5 class="main__title">Login</h5>
    </div>
    <?php if(session('message')): ?>
        <div class="alert alert-danger form-message">
            <i class="fas fa-volume-up"></i> <?= session('message') ?>
        </div>
    <?php endif; ?>
    <form action="<?= base_url('/login') ?>" method="POST" class="form-search">
        <?= csrf_field() ?>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" name="email" class="form-control <?= session('errors.email') ? 'is-invalid' : '' ?>" id="email" placeholder="Ingrese su dirección de correo electrónico" value="<?= old('email') ?>"  required>
            <div class="invalid-feedback"><?= session('errors.email') ?></div>
        </div>
        <div class="form-group">
            <label for="password">Contraseña</label>
            <input type="password" name="password" class="form-control <?= session('errors.password') ? 'is-invalid' : '' ?>" id="password" placeholder="Ingrese su contraseña de acceso" required>
            <div class="invalid-feedback"><?= session('errors.password') ?></div>
        </div>
        <div class="form-group">
            <button type="submit" name="send" class="btn btn-secondary btn-add float-right">Entrar</button>
            <div style="clear: both;"></div>
        </div>
    </form>
</div>

<?= $this->endSection() ?>
