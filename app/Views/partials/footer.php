<div class="container">
    <p class="footer__text">La página web de Antara Polanco es una obra creativa amparada por las leyes de protección de la propiedad intelectual, así como todos los elementos que la componen y es propiedad exclusiva de Antara Polanco. <a href="<?= base_url('terms_and_conditions') ?>" target="_blank" class="footer__link">Términos y Condiciones</a></p>
</div>
