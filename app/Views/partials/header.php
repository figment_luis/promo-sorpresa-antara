<div class="container">
    <div class="row">
        <div class="col-6">
            <h1 class="header__brand">
                <a href="<?= base_url() ?>" class="header__link" onclick="return false;">
                    <img src="<?= base_url('assets/img/logotipo_antara.jpg')  ?>" class="header__logotipo" alt="Logotipo Antara">
                    <span class="header__title">Antara</span>
                </a>
            </h1>
        </div>
        <div class="col-6">
            <?php if (session('admin_id')): ?>
                <li class="nav-item dropdown float-right">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: .9rem;">
                        <?= session('admin_fullname') ?>
                    </a>
                    <div class="dropdown-menu"
                        style="border: 0; text-align: right; padding: .2rem; border-radius: 0;">
                        <form action="<?= base_url('logout') ?>" method="post">
                            <button type="submit" class="btn-menu" name="send"><i class="fas fa-lock" style="color: #158F97;"></i> Cerrar Sesión</button>
                        </form>
                    </div>
                </li>
            <?php endif; ?>
        </div>
    </div>
</div>
