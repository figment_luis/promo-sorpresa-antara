<?= $this->extend('layout/app') ?>

<!-- Titulo de documento -->
<?= $this->section('title') ?>Registro de Redenciones<?= $this->endSection() ?>

<!-- Contenido principal -->
<?= $this->section('content') ?>
<div class="container" id="container-redemptions">
    <div class="row">
        <div class="col-md-6">
            <h5 class="main__title">Selección de Certificados</h5>
            <ul class="info-user">
                <li><strong>Nombre del Cliente:</strong> <?= $client_fullname ?></li>
                <li><strong>Certificados Registrados:</strong> <?= $current_redemptions ?> de <?= $redemptions_per_day ?></li>
                <li><strong>Monto Acumulado:</strong> $<?= number_format($total_amount, 2) ?></li>
                <li class="text-right"><i class="fas fa-gift info-highlight"></i><strong>Certificados Autorizados:</strong> <?= $redemptions_in_progress ?></li>
            </ul>
            <form id="form-certificates" class="mt-4">
                <div class="form-row">
                    <div class="col-md-9 mb-3">
                        <label for="store">Certificados Disponibles en el Sistema</label>
                        <select name="certificate" required class="custom-select" id="certificate" autofocus>
                            <option value="" selected disabled>Seleccione un certificado...</option>
                            <?php foreach ($certificates as $certificate): ?>
                                <option value="<?= $certificate['id'] ?>"><?= $certificate['name'] ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="col-md-3 mb-3 d-flex align-items-end">
                        <button class="btn btn-secondary btn-block btn-add" name="add" id="btn-add" type="submit">Agregar</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-6">
            <div class="title__container">
                <h5 class="main__title" style="margin-bottom: 0;">Certificados Otorgados</h5>
                <form action="<?= base_url('cancel-transaction') ?>" method="post" id="form-cancel-transaction">
                    <button type="submit" class="btn-danger btn btn-sm" title="Permite cancelar la operación actual de canje." name="send" id="btn-cancel"><i class="far fa-stop-circle" style="margin-right: .2rem;"></i> Cancelar Operación</button>
                </form>
            </div>
            <div class="info-concierge">
                <i class="fas fa-info-circle info-highlight"></i> <strong>Aviso importante</strong>
                <br>
                <br>
                Estimado administrador, recuerde que no existen saldos en las cuentas de los clientes, las cantidades que no alcancen para canjear un certificado, serán ignoradas.
            </div>
            <table class="table table-striped table-bordered table-sm table-tickets">
                <thead class="thead-dark text-center">
                    <tr>
                        <th>Cantidad</th>
                        <th>Nombre del Certificado</th>
                        <th>Fecha de Registro</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody id="table-redemptions">
                    <?php foreach ($redemptions as $redemption): ?>
                        <tr>
                            <td class="text-center"><?= $redemption['quantity'] ?></td>
                            <td><?= $redemption['name'] ?></td>
                            <td class="text-center"><?= $redemption['created_at'] ?></td>
                            <td class="text-center">
                                <a href="#" class="btn-delete" data-id="<?= $redemption['id'] ?>" title="Eliminar">
                                    <i class="fas fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
            <a href="<?= base_url('vouchers') ?>" class="btn btn-sm btn-next float-right"
                id="btn-next"
                style="display: none;">Canjear</a>
        </div>
    </div>



    <div class="row">
        <div class="col-md-10">

        </div>
        <div class="col-md-2">

        </div>
    </div>
</div>
<?= $this->endSection() ?>

<!-- Sección de de Hojas de estilo -->
<?= $this->section('scripts') ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<?= $this->endSection() ?>

<!-- Sección de scripts -->
<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/libs/jquery.number.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script>
    $(document).ready(function() {
        let baseUrl = '<?= base_url() ?>'

        // Verificar si el usuario puede canjear todos los certificados a los que tiene derecho
        let can_redeem = '<?= $can_redeem ?>';
        if (can_redeem) {
            $('#btn-next').show()
        }

        $('#form-certificates').submit(function(e) {
            e.preventDefault();
            // Deshabilitar botón de selección de certificados
            $('#btn-add').prop('disabled', true)
            $.ajax({
                url: baseUrl + '/redemptions',
                method: 'post',
                data: $(this).serialize(),
                headers: {'X-Requested-With': 'XMLHttpRequest'},
                dataType: 'json',
            })
            .done(function(data, textStatus, jqXHR) {
                console.log(data)
                if (data.status === 'success') {
                    toastr.success(data.message, 'Información del sistema', {timeOut: 1500})
                    // Recuperar el Foco del control de formulario
                    $('#certificate').val('').focus()
                    // Proceso de llenado: Tabla de certificados seleccionados
                    $('#table-redemptions tr').remove();
                    $.each(data.redemptions, function(index, value) {
                        $('#table-redemptions').append(`<tr><td class="text-center">${value.quantity}</td><td>${value.name}</td><td class="text-center">${value.created_at}</td><td class="text-center"><a href="#" class="btn-delete" data-id="${value.id}" title="Eliminar"><i class="fas fa-trash"></i></a></td></tr>`)
                    })
                    $('#certificate option').not(':disabled').remove();
                    // Proceso de llenado: DropDown de Certificados con stock disponible
                    $.each(data.certificates, function(index, value) {
                        $("#certificate").append(`<option value="${value.id}">${value.name}</option>`)
                    })

                    // Verificar si el usuario a seleccionado puede canjear todos los certificados a los que tiene derecho
                    if (data.can_redeem) {
                        $('#btn-next').show()
                    }

                } else if (data.status === 'error') {
                    // El usuario ha alcanzado el máximo número de redenciones permitidas por la transacción
                    Swal.fire({
                        icon: 'error',
                        title: 'Lo sentimos',
                        html: data.message,
                        confirmButtonColor: '#158f97',
                        confirmButtonText: 'Aceptar'
                    })
                } else {
                    // Una operación simultanea provocó que el certificado seleccionado no tenga existencias
                    // GANÓ LA OTRA OPERACIÓN
                    Swal.fire({
                        icon: 'error',
                        title: 'Lo sentimos',
                        html: data.message,
                        confirmButtonColor: '#158f97',
                        confirmButtonText: 'Aceptar'
                    })
                    // Actualizar el Dropdown con los certificados que aun tienen existencias
                    $('#certificate option').not(':disabled').remove();
                    // Proceso de llenado: DropDown de Certificados con stock disponible
                    $.each(data.certificates, function(index, value) {
                        $("#certificate").append(`<option value="${value.id}">${value.name}</option>`)
                    })
                    // Recuperar el Foco del control de formulario
                    $('#certificate').val('').focus()
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus)
                console.log(jqXHR.responseText)
            })
            .always(function() {
                // Habilitar nuevamente el botón de selección de certificados
                $('#btn-add').prop('disabled', false)
            })
        })

        $('body').delegate('.btn-delete', 'click', function(e) {
            e.preventDefault();
            $.ajax({
                url: baseUrl + '/redemptions/' + $(this).data('id'),
                method: 'post',
                headers: {'X-Requested-With': 'XMLHttpRequest'},
                dataType: 'json',
            })
            .done(function(data, textStatus, jqXHR) {
                console.log(data)
                 if (data.status === 'success') {
                    toastr.info(data.message, 'Información del sistema', {timeOut: 1500})
                    $('#table-redemptions tr').remove();
                    $.each(data.redemptions, function(index, value) {
                        $('#table-redemptions').append(`<tr><td class="text-center">${value.quantity}</td><td>${value.name}</td><td class="text-center">${value.created_at}</td><td class="text-center"><a href="#" class="btn-delete" data-id="${value.id}" title="Eliminar"><i class="fas fa-trash"></i></a></td></tr>`)
                    })

                    // Actualizar el Dropdown con los certificados que aun tienen existencias
                    $('#certificate option').not(':disabled').remove();
                    // Proceso de llenado: DropDown de Certificados con stock disponible
                    $.each(data.certificates, function(index, value) {
                        $("#certificate").append(`<option value="${value.id}">${value.name}</option>`)
                    })
                    // Recuperar el Foco del control de formulario
                    $('#certificate').val('').focus()

                    // Verificar si el usuario a seleccionado puede canjear todos los certificados a los que tiene derecho
                    if (!data.can_redeem) {
                        $('#btn-next').hide()
                    } else {
                        $('#btn-next').show()
                    }

                } else {
                    // La redención seleccionada no fue econtrada en el sistema para eliminarla (Muy raro)
                    Swal.fire({
                        icon: 'error',
                        title: 'Lo sentimos',
                        html: data.message,
                        confirmButtonColor: '#158f97',
                        confirmButtonText: 'Aceptar'
                    })
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus)
                console.log(jqXHR.responseText)
            }).always(function() {
                // Recuperar el foco del control de Formulario tras eliminar un certificado
                $('#certificate').val('').focus()
            })
        })

        $('#btn-next').click(function(e) {
            e.preventDefault();
            e.stopPropagation();
            // Deshabilitar el botón canjear
            $('#btn-next').prop('disabled', true)
            $.ajax({
                url: baseUrl + '/vouchers',
                method: 'post',
                // data: $(this).serialize(),
                headers: {'X-Requested-With': 'XMLHttpRequest'},
                dataType: 'json',
            })
            .done(function(data, textStatus, jqXHR) {
                console.log(data)
                if (data.status === 'success') {
                    // Limpiar el contenido de la pantalla
                    $('#container-redemptions').hide();
                    // Confirmar al usuario y redireccionar al Home
                    Swal.fire({
                        icon: 'success',
                        title: 'Felicidades',
                        text: data.message,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        confirmButtonColor: '#158f97',
                        confirmButtonText: 'Aceptar'
                    }).then(function() {
                        // Redireccionar al Concierge al Dashboard Administrativo.
                        //window.location.replace(baseUrl);
                        window.location = baseUrl + '/dashboard';
                    })

                } else {
                    // Fue imposible registrar el comprobante de canje
                    Swal.fire({
                        icon: 'error',
                        title: 'Lo sentimos',
                        text: data.message,
                        confirmButtonColor: '#158f97',
                        confirmButtonText: 'Aceptar'
                    })
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus)
                console.log(jqXHR.responseText)
            })
            .always(function() {
                // Habilitar nuevamente el botón canjear
                $('#btn-next').prop('disabled', false)
            })
        })

        $('#form-cancel-transaction').submit(function(e) {
            e.preventDefault();
            e.stopPropagation();
            // Deshabilitar el botón cancelar operación
            $('#btn-cancel').prop('disabled', true)
            Swal.fire({
                title: 'Alerta',
                text: "¿Realmente desea cancelar la operación?",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                showCancelButton: true,
                confirmButtonColor: '#158f97',
                cancelButtonColor: '#dc3545',
                confirmButtonText: 'Si, proceder a cancelar',
                cancelButtonText: 'No',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    // Solicitar al servidor que cancele la operación
                    $.ajax({
                        url: baseUrl + '/cancel-transaction',
                        method: 'post',
                        headers: {'X-Requested-With': 'XMLHttpRequest'},
                        dataType: 'json',
                    })
                    .done(function(data, textStatus, jqXHR) {
                        console.log(data)
                        if (data.status === 'success') {
                            // Confirmar al usuario y redireccionar al Dashboard
                            Swal.fire({
                                icon: 'success',
                                title: 'Felicidades',
                                text: data.message,
                                allowOutsideClick: false,
                                allowEscapeKey: false,
                                confirmButtonColor: '#158f97',
                                confirmButtonText: 'Aceptar'
                            }).then(function() {
                                // Redireccionar al Concierge al Dashboard Administrativo.
                                //window.location.replace(baseUrl);
                                window.location = baseUrl + '/dashboard';
                            })

                        } else {
                            // Fue imposible cancelar la operación
                            Swal.fire({
                                icon: 'error',
                                title: 'Lo sentimos',
                                text: data.message,
                                confirmButtonColor: '#158f97',
                                confirmButtonText: 'Aceptar'
                            })
                        }
                    })
                    .fail(function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus)
                        console.log(jqXHR.responseText)
                    })
                    .always(function() {
                        // Habilitar nuevamente el botón canjear
                        $('#btn-cancel').prop('disabled', false)
                    })
                } else if ( result.dismiss === Swal.DismissReason.cancel) {
                    $('#btn-cancel').prop('disabled', false)
                }
            })
        })
    })

</script>
<?= $this->endSection() ?>
