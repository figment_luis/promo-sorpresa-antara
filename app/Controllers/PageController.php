<?php namespace App\Controllers;

class PageController extends BaseController
{

    public function terms()
    {
        return view('pages/terms_and_conditions');
    }

    public function report()
    {
    	// Detectar el tipo de fin de linea segun el sistema operativo
    	ini_set("auto_detect_line_endings", true);
    
    	$db = db_connect();

    	$sql = "SELECT CONCAT(users.first_name, ' ', users.last_name) AS 'Nombre Cliente', users.email AS 'Email', redemptions.quantity AS 'Cantidad', certificates.name AS 'Nombre del Certificado', redemptions.created_at AS 'Fecha de Operación' FROM users INNER JOIN redemptions ON redemptions.user_id = users.id INNER JOIN certificates ON certificates.id = redemptions.certificate_id WHERE users.role_id = 2";

        $results = $db->query($sql)->getResultArray();
        
        $filename =  "reporte_". date('Y-m-d') .".csv";

        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=\"$filename\"");

        $fh = fopen( 'php://output', 'w' );
    	$heading = false;

    	if(!empty($results)) {
          foreach($results as $row) {
            if(!$heading) {
              // Colocar el nombre de las cabeceras
              fputcsv($fh, array_keys($row));
              $heading = true;
            }
            // Insertar cada uno de los registros dentro de CSV
            fputcsv($fh, array_values($row));

          }
          // Cerrar el stream 
          fclose($fh);
          exit();
      	}
    }

	//--------------------------------------------------------------------

}
