<?php
namespace App\Controllers;

class TicketController extends BaseController
{

    public function __construct()
    {
        $session = session();

        // Proteger todos los métodos de este controlador de accesos no autorizados (solo usuarios logeados)
        if (!$session->get('admin_id')) {
            header('Location: ' . base_url('login'));
            exit;
        }

        // Los métodos de este controlador solo estan disponibles si en sesión hay un ID de cliente seleccionado
        if (!$session->get('client_id')) {
            header('Location: ' . base_url('search'));
            exit;
        }
    }

    public function create()
    {
        $storeModel = model('StoreModel');
        $ticketModel = model('TicketModel');
        $session = session();

        // Fix 2020-12-18
        // Ordenar las tiendas por orden alfabético
        $stores = $storeModel->orderBy('name', 'ASC')->findAll();

        // Recuperar los posibles tickets almacenados en la sessión (refresh accidental)
        $tickets = [];
        if(count($session->get('tickets'))) {
            // $tickets = $ticketModel->whereIn('id',  $session->get('tickets'))->get()->getResultArray();
            //
            // La siguiente consulta muestra el nombre de la tienda que emitió el ticket
            $tickets = $ticketModel
                        ->select('tickets.id, tickets.number, tickets.amount, tickets.date, stores.name as store')
                        ->join('stores', 'tickets.store_id = stores.id')
                        ->whereIn('tickets.id', $session->get('tickets'))
                        ->get()
                        ->getResultArray();
        }

        return view('tickets/create', [
            'client_fullname' => $session->get('client_fullname'),
            'current_redemptions' => $session->get('current_redemptions'),
            'redemptions_per_day' => $session->get('redemptions_per_day'),
            'total_amount' => $session->get('total_amount'),
            'stores' => $stores,
            'tickets' => $tickets,
        ]);
    }

    public function store()
    {
        if ($this->request->isAJAX()) {
            $ticketModel = model('TicketModel');
            $session = session();

            // Verificar que la fecha del ticket de compra coincida con el día en que se lleva a cabo la transacción
            // Tickets de compra con fechas diferentes al día de hoy, no son acumulables.
            $purchase_date = date('Y-m-d', strtotime($this->request->getPost('date')));

            if ($purchase_date !== date('Y-m-d')) {
                $response = [
                    'status' => 'error',
                    'message' => 'El Ticket número <b>' . $this->request->getPost('number') . '</b> venció el día <br>'. strftime('%d de %B de %Y', strtotime($purchase_date)),
                ];
                return json_encode($response);
            }

            // Verificar que el ticket de compra no se encuentre previamente registrado en el sistema
            // Número de Ticket, Tienda, Fecha/Hora de Compra y Monto de Compra:
            // Puede haber datos repetidos, pero no los cuatro.
            $ticketFound = count($ticketModel->where('number', $this->request->getPost('number'))
                                       ->where('store_id', $this->request->getPost('store'))
                                       ->where('amount', $this->request->getPost('amount'))
                                       ->where('date', $this->request->getPost('date'))
                                       ->get()
                                       ->getResultArray());

            if ($ticketFound > 0) {
                $response = [
                    'status' => 'error',
                    'message' => 'El Ticket número <b>' . $this->request->getPost('number') . '</b> con fecha<br>' . strftime('%d de %B de %Y', strtotime($purchase_date)) . '<br>Ya fue registrado previamente en el sistema.',
                ];
                return json_encode($response);
            }

            // Proceder a registrar el ticket de compra
            $ticketID = $ticketModel->insert([
                'number' => $this->request->getPost('number'),
                'date' => $this->request->getPost('date'),
                'amount' => $this->request->getPost('amount'),
                'store_id' => $this->request->getPost('store'),
                'user_id' => $session->get('client_id'),
                'admin_id' => $session->get('admin_id'),
            ]);

            // Otorgar un premio (certificado) por cada $5000 de compra
            $purchase_requirement = 5000;

            // Guardar en sessión el ID del ticket registrado (los tickets son acumulables durante la transacción actual)
            $session->push('tickets', [$ticketID]);

            // FIX: 2020-12-15
            // Si el monto de un ticket es superior a 99999999.99, la base de datos colocará este dato como valor del campo amount (lo redondea al valor más bajo que sería 99999999,99). 
            // Lo que provocaría que la sumatoria falle, pues el valor real (que da el usuario) se tiene en la sesion (gui), pero el persistido (redondeado) se encuentra en la base de datos.
            // Por consiguiente, al momento de querer eliminar un ticket, la resta falla, pues la sumatoria no es identica a la que se tiene en sesion y la base de datos.
            // 
            // NOTA:    Este bug se presenta generalmente cuando el operador del sistema ingresa un numero de ticket (mayor a 99999999.99) como monto. Lo que provoca el desfase.
            //          Eliminando el ticket no se resuelve el problema, pues el desfase persiste, lo mejor sería cancelar la operación


            // Recuperar el monto total del ticket registrado en la base de datos
            $user_amount = $ticketModel->where('id', $ticketID)->get()->getRow()->amount;


            // Guardar en session el monto total acumulado de los tickets registrados
            // $total_amount = $session->get('total_amount') + $this->request->getPost('amount');
            $total_amount = $session->get('total_amount') + $user_amount;

            $session->set('total_amount', $total_amount);

            // Guardar en sessión el número de redenciones en progreso
            // La cantidad de premios que puede canjear con el monto acumulado de sus tickets registrados en esta transacción
            $redemptions_in_progress = floor($total_amount / $purchase_requirement);
            $redemptions_in_progress = $redemptions_in_progress > $session->get('redemptions_available') ? $session->get('redemptions_available') : $redemptions_in_progress;
            $session->set('redemptions_in_progress', $redemptions_in_progress);

            // Recuperar el listado de tickets registrados en esta transacción
            // $tickets = $ticketModel->whereIn('id', $session->get('tickets'))->get()->getResultArray();
            //
            // La siguiente consulta muestra el nombre de la tienda que emitió el ticket
            $tickets = $ticketModel
                        ->select('tickets.id, tickets.number, tickets.amount, tickets.date, stores.name as store')
                        ->join('stores', 'tickets.store_id = stores.id')
                        ->whereIn('tickets.id', $session->get('tickets'))
                        ->get()
                        ->getResultArray();

            $response = [
                'status' => 'success',
                'message' => 'Ticket ' . $this->request->getPost('number') . ' registrado correctamente en el sistema.',
                'ticket_id' => $ticketID,
                'tickets' => $tickets,
                'total_amount' => $total_amount,
            ];

            return json_encode($response);
        } else {
            echo 'Acceso vía ajax';
        }
    }

    public function destroy($id)
    {
        if ($this->request->isAJAX()) {
            $ticketModel = model('TicketModel');
            $session = session();

            // Localizar el ticket a eliminar
            $ticketFound = $ticketModel->find($id);

            if (isset($ticketFound)) {

                 // Recuperar el monto total del ticket a eliminar de la transacción
                $remove_amount = $ticketModel->where('id', $id)->get()->getRow()->amount;

                $ticketModel->delete($id, true);

                // Actualizar la sessión retirando el ID del ticket eliminado
                $key = array_search($id, $session->get('tickets'));

                if ($key !== false) {

                    // Otorgar un premio (certificado) por cada $5000 de compra
                    $purchase_requirement = 5000;

                    // Recuperar el arreglo de tickets almacenados en la sessión
                    $current_tickets = $session->get('tickets');

                    // Eliminar el ID del ticket registrado en sessión
                    unset($current_tickets[$key]);
                    $session->set('tickets', $current_tickets);

                    // Actualizar el monto total de los tickets registrados en la session
                    $total_amount = $session->get('total_amount') - $remove_amount;

                    // Actualizar las redenciones en progreso
                    // Determinar si se puede o no canjear premios con el nuevo monto acumulado
                    // FIX ====== Error de cálculo de punto flotante ===== PHP coloca un número negativo muy pequeño cuando el resultado de la operación anterior es cero
                    if ($total_amount <= 0) {
                        $total_amount = 0;
                        $redemptions_in_progress = 0;
                    } else {
                        $redemptions_in_progress = floor($total_amount / $purchase_requirement);
                        $redemptions_in_progress = $redemptions_in_progress > $session->get('redemptions_available') ? $session->get('redemptions_available') : $redemptions_in_progress;
                    }

                    // Actualizar en sessión el nuevo monto total acumulado
                    $session->set('total_amount', $total_amount);

                    // Almacenar en sessión las nuevas redenciones disponibles para esta transacción
                    $session->set('redemptions_in_progress', $redemptions_in_progress);
                }

                // Recuperar los posibles tickets almacenados en la sessión
                $tickets = [];
                if(count($session->get('tickets'))) {
                    // $tickets = $ticketModel->whereIn('id',  $session->get('tickets'))->get()->getResultArray();
                    //
                    // La siguiente consulta muestra el nombre de la tienda que emitió el ticket
                    $tickets = $ticketModel
                        ->select('tickets.id, tickets.number, tickets.amount, tickets.date, stores.name as store')
                        ->join('stores', 'tickets.store_id = stores.id')
                        ->whereIn('tickets.id', $session->get('tickets'))
                        ->get()
                        ->getResultArray();
                }

                $response = [
                    'status' => 'success',
                    'message' => 'El Ticket ' . $ticketFound['number'] . ' fue eliminado correctamente del sistema',
                    'ticket_id' => $ticketFound,
                    'tickets' => $tickets,
                    'remove_amount' => $remove_amount,
                    'total_amount' => $session->get('total_amount'),
                ];

                return json_encode($response);
            } else {

                $response = [
                    'status' => 'error',
                    'message' => 'Error del sistema, no fue posible eliminar el Ticket seleccionado',
                    'ticketFound' => $ticketFound,
                ];

                return json_encode($response);
            }
        } else {
            echo 'Acceso vía Ajax';
        }
    }
}
