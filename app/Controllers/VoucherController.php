<?php
namespace App\Controllers;

class VoucherController extends BaseController
{

    public function __construct()
    {
        $session = session();

        // Proteger todos los métodos de este controlador de accesos no autorizados (solo usuarios logeados)
        if (!$session->get('admin_id')) {
            header('Location: ' . base_url('login'));
            exit;
        }
    }

    public function print()
    {
        if ($this->request->isAJAX()) {
            $voucherModel = model('VoucherModel');
            $session = session();

            // Proceder a registrar el comprobante de canje
            $voucherID = $voucherModel->insert([
                'total_amount' => $session->get('total_amount'),
                'tickets' => implode(',', $session->get('tickets')),
                'redemptions' => implode(',', $session->get('redemptions')),
                'user_id' => $session->get('client_id'),
                'admin_id' => $session->get('admin_id'),
            ]);

            if ($voucherID === 0) {
                // Error al registrar el voucher
                $response = [
                    'status' => 'error',
                    'message' => 'Imposible registrar el comprobante en el sistema',
                    'voucher_id' => $voucherID,
                ];
                return json_encode($response);
            }

            // Eliminar las variables de sessión para el cliente actual (solo dejar las del administrador "Concierge")
            $session->remove('client_id');
            $session->remove('client_fullname');
            $session->remove('redemptions');
            $session->remove('tickets');
            $session->remove('redemptions_per_day');
            $session->remove('current_redemptions');
            $session->remove('redemptions_available');
            $session->remove('redemptions_in_progress');
            $session->remove('current_certificates');
            $session->remove('total_amount');
            $session->remove('can_redeem');

            $response = [
                'status' => 'success',
                'message' => 'Comprobante registrado satisfactoriamente en el sistema',
                'voucher_id' => $voucherID,
            ];

            return json_encode($response);

        } else {
            echo 'Acceso permitido solo via ajax';
        }
    }

}
