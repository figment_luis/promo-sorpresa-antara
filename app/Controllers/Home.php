<?php namespace App\Controllers;

class Home extends BaseController
{
    public function __construct()
    {
        $session = session();

        // Proteger todos los métodos de este controlador de accesos no autorizados (solo usuarios logeados)
        if (!$session->get('admin_id')) {
            header('Location: ' . base_url('login'));
            exit;
        }
    }

    public function dashboard()
    {
        $session = session();

        return view('dashboard/index', [
            'admin_fullname' => $session->get('admin_fullname'),
        ]);
    }

	//--------------------------------------------------------------------

}
