<?php namespace App\Controllers;

class TransactionController extends BaseController
{
	public function cancel()
	{
        if ($this->request->isAJAX()) {
            $session = session();

            // Proteger todos los métodos de este controlador de accesos no autorizados (solo usuarios logeados)
            if (!$session->get('admin_id')) {
                header('Location: ' . base_url('login'));
                exit;
            }

            $ticketModel = model('TicketModel');
            $redemptionModel = model('RedemptionModel');
            $db = \Config\Database::connect();

            // Ejecutar transacción para la cancelación de la operación
            // Tablas afectadas: 'tickets', 'redemptions' y 'certificates'
            $db->transBegin();

            // Verificar si en la sessión existe la variable tickets
            if ($session->get('tickets')) {

                // Verificar si existe por lo menos un ticket (id) almacenado en la sessión
                if (count($session->get('tickets'))) {

                    // Eliminar los tickets de la base de datos
                    // Mismos que se relacionen con los ID del listado de tickets almacenados en sessión
                    $ticketModel->delete($session->get('tickets'), true);
                }

            }

            // Verificar si existen rendeciones registradas en sesión
            if ($session->get('redemptions')) {

                // Verificar si existe por lo menos una redención (canje de regalo) almacenado en la sessión
                if (count($session->get('redemptions'))) {

                    // 1. Obtener los ID de los certificados asociados con las redenciones a eliminar

                    // TEMA A DEBATIR:
                    // ¿Porqué en esta consulta no funciona el whereIn, si en los otros modelos incluyendo este declarado en un controlador diferente funciona correctamente?
                    // $redemptions = $redemptionModel->findColumn('id', $session->get('redemptions'));
                    $redemptions = $redemptionModel->find($session->get('redemptions'));
                    //$redemptions = $redemptionModel->findColumn('certificate_id',[23,24]);
                    // var_dump($redemptions);
                    //var_dump($session->get('redemptions'));
                    // 2. Actualizar las existencias +1 e intercambios -1 de cada uno de los certificados asociados
                    $builder = $db->table('certificates');
                    /*$certificateUpdated = $builder->set('current_stock', 'current_stock + 1', false)
                                                  ->set('exchanges', 'exchanges - 1', false)
                                                  ->set('updated_at', 'NOW()', FALSE)
                                                  ->whereIn('id', $redemptions)
                                                  ->update();*/
                    foreach ($redemptions as $my_redemption) {
                        $builder->set('current_stock', 'current_stock + 1', false)
                                                  ->set('exchanges', 'exchanges - 1', false)
                                                  ->set('updated_at', 'NOW()', FALSE)
                                                  ->where('id', $my_redemption['certificate_id'])
                                                  ->update();
                    }

                    // Eliminar las redenciones de la base de datos.
                    // Mismos que se relacionen con los ID del listado de redenciones almacenados en sessión
                    $redemptionModel->delete($session->get('redemptions'), true);
                }
            }

            // Verificar si la transacción de cancelación de operaciones fué exitosa
            if ($db->transStatus() === FALSE) {
                // Deshacer los cambios efectuados en las tablas redemptions, certificates y tickets
                $db->transRollback();

                $response = [
                    'status' => 'error',
                    'message' => 'Error del sistema: No fue posible cancelar esta operación. Intente nuevamente.',
                ];

                return json_encode($response);

            } else {
                // Confirmar cambios en la base de datos
                $db->transCommit();

                // Eliminar todas las variables de session del cliente actual (solo dejar las del administrador "Concierge")
                $session->remove('client_id');
                $session->remove('client_fullname');
                $session->remove('redemptions');
                $session->remove('tickets');
                $session->remove('redemptions_per_day');
                $session->remove('current_redemptions');
                $session->remove('redemptions_available');
                $session->remove('redemptions_in_progress');
                $session->remove('current_certificates');
                $session->remove('total_amount');
                $session->remove('can_redeem');

                $response = [
                    'status' => 'success',
                    'message' => 'Operación cancelada satisfactoriamente.',
                ];

                return json_encode($response);
            }

        } else {
            echo 'Operación solo vía ajax';
        }
	}


	//--------------------------------------------------------------------

}
