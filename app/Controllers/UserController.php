<?php
namespace App\Controllers;

class UserController extends BaseController
{

    public function __construct()
    {
        $session = session();

        // Proteger todos los métodos de este controlador de accesos no autorizados (solo usuarios logeados)
        if (!$session->get('admin_id')) {
            header('Location: ' . base_url('login'));
            exit;
        }
    }

    public function create()
    {
        // Formulario de registro de clientes
        return view('users/create');
    }

    public function store()
    {
        $userModel = model('UserModel');
        $validation = service('validation');

        // Comprobar que los datos cumplan con las reglas de validación
        if(!$validation->withRequest($this->request)->run(null, 'signup')) {
            return redirect()->back()->withInput()->with('errors', $validation->getErrors());
        }

        // Proceder a registrar el usuario (retorna id del registro actual)
        $userID = $userModel->insert([
            'first_name' => $this->request->getPost('first_name'),
            'last_name' => $this->request->getPost('last_name'),
            'email' => $this->request->getPost('email'),
            'phone' => $this->request->getPost('phone'),
        ]);

        if ($userID === 0) {
            return redirect()->back()->withInput()->with('errors', 'Error 500, No fue posible registrar el cliente en el sistema');
        }

        // TODO     ¿Como verificar que el usuario no se registre más de una vez en el sistema con diferentes datos?
        // Analisis biometrico :)

        $redemptions_per_day = 3;       // Máximo número de premios (certificados) a canjear en el día
        $current_redemptions = 0;       // Número de premios canjeados en este día
        $redemptions_available = 3;      // Número de premios que aun puedo canjear en el día

        // Guardar en sessión los datos del nuevo cliente para llevar a cabo la trasacción (promoción)
        // ID, Nombre completo, redenciones_por_dia,
        // redenciones_actuales (realizadas previamente en este día), redenciones disponibles,
        // redenciones en progreso (por acumulamiento de tickets registrados),
        // certificados actuales (número de certificados seleccionados durante el canje),
        // monto total de los tickets registrados
        $sessionData = [
            'client_id' => $userID,
            'client_fullname' => $this->request->getPost('first_name') . ' ' . $this->request->getPost('last_name'),
            'tickets' => [],
            'redemptions' => [],
            'redemptions_per_day' => $redemptions_per_day,
            'current_redemptions' => $current_redemptions,
            'redemptions_available' => $redemptions_available,
            'redemptions_in_progress' => 0,
            'current_certificates' => 0,
            'total_amount' => 0,
            'can_redeem' => false,
        ];
        $session = session();
        $session->set($sessionData);

        return redirect()->route('tickets.create');

    }

    public function search()
    {
        // Formulario de búsqueda de clientes registrados en el sistema
        return view('users/search');
    }

    public function find()
    {
        $userModel = model('UserModel');
        $validation = service('validation');
        $db = \Config\Database::connect();

        // Por defecto, la búsqueda de clientes es por email
        $field = $this->request->getPost('field') ?: 'email';

        // Comprobar que el dato ingresado cumpla con las reglas de validación (sea email | teléfono)
        if(!$validation->withRequest($this->request)->run(null, $field)) {
            return redirect()->back()->withInput()->with('errors', $validation->getErrors());
        }

        // Localizar el cliente por el campo seleccionado (email | phone) en la caja de búsqueda
        $builder = $db->table('users');
        $user = $builder->where($field, $this->request->getPost('query'))
                        ->where('role_id', 2)
                        ->get()
                        ->getRow();

        if(isset($user)) {
            $builderRedemption = $db->table('redemptions');

            // Máximo número de premios a canjear en el día
            $redemptions_per_day = 3;

            // Número de premios canjeados en este día
            $current_redemptions = count($builderRedemption->where('user_id', $user->id)
                                                            ->where('YEAR(created_at)', date('Y'))
                                                            ->where('MONTH(created_at)', date('m'))
                                                            ->where('DAY(created_at)', date('d'))
                                                            ->get()
                                                            ->getResultArray());

            // Número de premios que aun puede canjear en el día
            $redemptions_available = $redemptions_per_day - $current_redemptions;

            // Redireccionar al usuario si el cliente ha alcanzado el máximo número de redenciones en el día
            if ($redemptions_available === 0) {
                return redirect()->back()->withInput()->with('message', 'El cliente seleccionado ha alcanzado el máximo número de redenciones permitidas en este día.');
            }

            // Guardar en sessión los datos del nuevo cliente para llevar a cabo la trasacción (promoción)
            // ID, Nombre completo, redenciones_por_dia,
            // redenciones_actuales (realizadas previamente en este día), redenciones disponibles,
            // redenciones en progreso (por acumulamiento de tickets registrados),
            // certificados actuales (número de certificados seleccionados durante el canje),
            // monto total de los tickets registrados
            $sessionData = [
                'client_id' => $user->id,
                'client_fullname' => $user->first_name . ' ' . $user->last_name,
                'tickets' => [],
                'redemptions' => [],
                'redemptions_per_day' => $redemptions_per_day,
                'current_redemptions' => $current_redemptions,
                'redemptions_available' => $redemptions_available,
                'redemptions_in_progress' => 0,
                'current_certificates' => 0,
                'total_amount' => 0,
                'can_redeem' => false,
            ];
            $session = session();
            $session->set($sessionData);

            return redirect()->route('tickets.create');
        } else {
            return redirect()->back()->withInput()->with('message', 'Lo sentimos, el cliente no fue localizado en la base de datos.');
        }
    }

}
