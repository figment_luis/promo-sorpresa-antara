<?php namespace App\Controllers;

class AuthController extends BaseController
{

	public function login()
	{

        $session = session();
        // Proteger método de accesos no autorizados (solo usuarios invitados)
        if ($session->get('admin_id')) {
            header('Location: ' . base_url('dashboard'));
            exit;
        }

		return view('auth/login');
	}

    public function aLogin()
    {
        $session = session();

        // Proteger método de accesos no autorizados (solo usuarios invitados)
        if ($session->get('admin_id')) {
            header('Location: ' . base_url('dashboard'));
            exit;
        }

        $userModel = model('UserModel');
        $validation = service('validation');
        $db = \Config\Database::connect();

        // Comprobar que los datos cumplan con las reglas de validación
        if(!$validation->withRequest($this->request)->run(null, 'login')) {
            return redirect()->back()->withInput()->with('errors', $validation->getErrors());
        }

        // Localizar el usuario (POR EMAIL) con rol de administrador en la base de datos
        $builder = $db->table('users');
        $user = $builder->where('email', $this->request->getPost('email'))
                        ->where('role_id', 1)
                        ->get()
                        ->getRow();

        if (isset($user)) {

            // Cotejar passwords [user vs database]
            if (password_verify($this->request->getPost('password'), $user->password)) {

                // Guardar en session el id y nombre del administrador actualmente logeado
                $sessionData = [
                    'admin_id' => $user->id,
                    'admin_fullname' => $user->first_name . ' ' . $user->last_name,
                ];
                $session->set($sessionData);

                return redirect()->route('dashboard');
            } else {
                // Contraseña incorrecta
                return redirect()->back()->withInput()->with('message', 'Lo sentimos, el usuario no fue localizado en la base de datos.<br>Intente con las credenciales correctas');
            }

        } else {
            // Email no localizado en la base de datos
            return redirect()->back()->withInput()->with('message', 'Lo sentimos, el usuario no fue localizado en la base de datos.<br>Intente con las credenciales correctas');
        }
    }

    public function logout()
    {

        $session = session();

        // Proteger métodos de accesos no autorizados (solo usuarios logeados)
        if (!$session->get('admin_id')) {
            header('Location: ' . base_url('login'));
            exit;
        }

        // Cancelar cualquier operación de canje en proceso
        if($this->cancelOperation()) {

            // Destruir cualquier dato almacenado en la sesión (administrador | cliente)
            $session->destroy();
            return redirect()->route('auth.login');
        } else {
            return redirect()->back()->with('message', 'Surgió un error inesperado en el sistema, le recomendamos cerrar cualquier operación activa y posteriormente volver a intentar cerrar su sesión.');
        }

    }
	//--------------------------------------------------------------------

    private function cancelOperation()
    {
        $ticketModel = model('TicketModel');
        $redemptionModel = model('RedemptionModel');
        $db = \Config\Database::connect();

        $session = session();

        // Ejecutar transacción para la cancelación de la operación
        // Tablas afectadas: 'tickets', 'redemptions' y 'certificates'
        $db->transBegin();

        // Verificar si en la sessión existe la variable tickets
        if ($session->get('tickets')) {

            // Verificar si existe por lo menos un ticket (id) almacenado en la sessión
            if (count($session->get('tickets'))) {

                // Eliminar los tickets de la base de datos
                // Mismos que se relacionen con los ID del listado de tickets almacenados en sessión
                $ticketModel->delete($session->get('tickets'), true);
            }

        }

        // Verificar si existen rendeciones registradas en sesión
        if ($session->get('redemptions')) {

            // Verificar si existe por lo menos una redención (canje de regalo) almacenado en la sessión
            if (count($session->get('redemptions'))) {

                // 1. Obtener los ID de los certificados asociados con las redenciones a eliminar

                // TEMA A DEBATIR:
                // ¿Porqué en esta consulta no funciona el whereIn, si en los otros modelos incluyendo este declarado en un controlador diferente funciona correctamente?
                // $redemptions = $redemptionModel->findColumn('certificate_id', $session->get('redemptions'));
                $redemptions = $redemptionModel->find($session->get('redemptions'));

                // 2. Actualizar las existencias +1 e intercambios -1 de cada uno de los certificados asociados
                $builder = $db->table('certificates');
                /*$certificateUpdated = $builder->set('current_stock', 'current_stock + 1', false)
                                              ->set('exchanges', 'exchanges - 1', false)
                                              ->set('updated_at', 'NOW()', FALSE)
                                              ->whereIn('id', $redemptions)
                                              ->update();*/

                foreach ($redemptions as $my_redemption) {
                    $builder->set('current_stock', 'current_stock + 1', false)
                                                  ->set('exchanges', 'exchanges - 1', false)
                                                  ->set('updated_at', 'NOW()', FALSE)
                                                  ->where('id', $my_redemption['certificate_id'])
                                                  ->update();
                }

                // Eliminar las redenciones de la base de datos.
                // Mismos que se relacionen con los ID del listado de redenciones almacenados en sessión
                $redemptionModel->delete($session->get('redemptions'), true);
            }
        }

        $status = null;
        // Verificar si la transacción de cancelación de operaciones fué exitosa
        if ($db->transStatus() === FALSE) {
            // Deshacer los cambios efectuados en las tablas redemptions, certificates y tickets
            $db->transRollback();
            $status = false;
        } else {
            // Confirmar cambios en la base de datos
            $db->transCommit();
            $status = true;
        }
        return $status;
    }
}
