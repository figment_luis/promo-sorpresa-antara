<?php
namespace App\Controllers;

class RedemptionController extends BaseController
{

    public function __construct()
    {
        $session = session();

        // Proteger todos los métodos de este controlador de accesos no autorizados (solo usuarios logeados)
        if (!$session->get('admin_id')) {
            header('Location: ' . base_url('login'));
            exit;
        }

        // Los métodos de este controlador solo estan disponibles si en sesión hay un ID de cliente seleccionado
        if (!$session->get('client_id')) {
            header('Location: ' . base_url('search'));
            exit;
        }

    }

    public function create()
    {
        $certificateModel = model('CertificateModel');
        $redemptionModel = model('RedemptionModel');
        $session = session();

        // Localizar todos los certificados que tengan stock en el sistema
        $certificates = $certificateModel->where('current_stock >', 0)->get()->getResultArray();

        // Recuperar las posibles redenciones "certificados seleccionados" almacenados en la sessión (refresh accidental)
        $redemptions = [];
        if(count($session->get('redemptions'))) {
            $redemptions = $redemptionModel
                            ->select('redemptions.id, redemptions.quantity, redemptions.created_at, users.first_name, users.last_name, certificates.name')
                            ->join('users', 'redemptions.user_id = users.id')
                            ->join('certificates', 'redemptions.certificate_id = certificates.id')
                            ->whereIn('redemptions.id',  $session->get('redemptions'))
                            ->get()
                            ->getResultArray();
        }

        // Verificar si el usuario puede proceder a canjear sus certificados (Activar botón canjear)
        //
        // Recalcular a consecuencia de una actualización en tickets.
        //      El Concierge pudo haber regresado a la sección de tickets y en consecuencia
        //      Ingreso ó Eliminó algunos de ellos, lo que afecta a la cantidad de regalos permitidos.
        $can_redeem = $session->get('current_certificates') == $session->get('redemptions_in_progress');

        // Un usuario puede canjear regalos solo si el monto acumulado de sus tickets es superior o igual a $5000
        if ($session->get('redemptions_in_progress') == 0) {
            $session->set('can_redeem', false);
        } else {
            // $can_redeem puede ser falso | verdadero
            // Depende si los certificados seleccionados son identicos a la cantidad de redenciones permitidas en la transacción
            $session->set('can_redeem', $can_redeem);
        }

        return view('redemptions/create', [
            'client_fullname' => $session->get('client_fullname'),
            'current_redemptions' => $session->get('current_redemptions'),
            'redemptions_per_day' => $session->get('redemptions_per_day'),
            'redemptions_in_progress' => $session->get('redemptions_in_progress'),
            'total_amount' => $session->get('total_amount'),
            'redemptions' => $redemptions,
            'certificates' => $certificates,
            'can_redeem' => $session->get('can_redeem'),
        ]);
    }

    public function store()
    {
        if ($this->request->isAJAX()) {
            $redemptionModel = model('RedemptionModel');
            $certificateModel = Model('CertificateModel');
            $db = \Config\Database::connect();

            $session = session();

            // Verificar que los certificados actualmente seleccionados no superen en cantidad
            //      al máximo número de redenciones permitidas para esta transacción
            if ($session->get('current_certificates') < $session->get('redemptions_in_progress')) {

                // FIX - 2020-12-10
                // Verificar si el certificado actualmente seleccionado, tiene existencias suficientes.
                // El sistema será operado de forma concurrente (bajo demanda).

                $current_certificate = $certificateModel->find($this->request->getPost('certificate'));
                if(isset($current_certificate)) {
                    if($current_certificate['current_stock'] <= 0) {
                         // Localizar todos los certificados que tengan stock en el sistema
                        $certificates = $certificateModel->where('current_stock >', 0)->get()->getResultArray();

                        $response = [
                        'status' => 'insufficient stock',
                        'message' => 'Otra operación simultanea provocó que el certificado seleccionado tenga existencias insuficientes en el sistema.<br><b>Por favor, seleccione otro certificado.</b>',
                        'current_certificate' => $current_certificate,
                        'certificates' => $certificates
                    ];

                    return json_encode($response);
                    }
                }


                // Ejecutar transacción de canje (Crear Redención y Actualizar el Stock del Certificado seleccionado)
                $db->transBegin();

                // Proceder a registrar la redención por canje de certificado seleccionado
                $redemptionID = $redemptionModel->insert([
                    'quantity' => 1,
                    'certificate_id' => $this->request->getPost('certificate'),
                    'user_id' => $session->get('client_id'),
                    'admin_id' => $session->get('admin_id'),
                ]);

                // Actualizar el current_stock del certificado seleccionado en -1
                // Actualizar el número de veces que este producto se ha intercambiado
                // BUG ====== ModelCertificate ocasiona un error al tratar de escapar los datos con SET
                $builder = $db->table('certificates');
                $certificateUpdated = $builder->set('current_stock', 'current_stock - 1', false)
                                              ->set('exchanges', 'exchanges + 1', false)
                                              ->set('updated_at', 'NOW()', FALSE)
                                              ->where('id', $this->request->getPost('certificate'))
                                              ->update();

                // Verificar si la transacción de canje fué exitosa
                if ($db->transStatus() === FALSE) {

                    // Deshacer los cambios efectuados en las tablas redemptions y certificates
                    $db->transRollback();
                    $response = [
                        'status' => 'error',
                        'message' => 'Error del sistema: No fue posible realizar la transacción de selección de certificados. Intente nuevamente.',
                        'current_certificates' => $session->get('current_certificates'),
                    ];

                    return json_encode($response);

                } else {
                    $db->transCommit();

                    // Guardar en sessión el ID de la redención registrada
                    $session->push('redemptions', [$redemptionID]);

                    // Guardar en sessión el número de redenciones actualmente seleccionadas
                    // La cantidad de certificados agregados a la bolsa de regalos
                    $current_certificates = $session->get('current_certificates') + 1;
                    $session->set('current_certificates', $current_certificates);

                    // Verificar si el cliente puede proceder a canjear sus certificados (activar botón Canjear)
                    // Los certificados seleccionados deben sear iguales al número de redenciones permitidas en esta transacción
                    $can_redeem = $session->get('current_certificates') == $session->get('redemptions_in_progress');
                    $session->set('can_redeem', $can_redeem);

                    // Localizar todos los certificados que tengan stock en el sistema
                    $certificates = $certificateModel->where('current_stock >', 0)->get()->getResultArray();

                    // Recuperar el listado de redenciones (certificados) seleccionadas actualmente por el usuario
                    $redemptions = $redemptionModel
                                    ->select('redemptions.id, redemptions.quantity, redemptions.created_at, users.first_name, users.last_name, certificates.name')
                                    ->join('users', 'redemptions.user_id = users.id')
                                    ->join('certificates', 'redemptions.certificate_id = certificates.id')
                                    ->whereIn('redemptions.id',  $session->get('redemptions'))
                                    ->get()
                                    ->getResultArray();

                    $response = [
                        'status' => 'success',
                        'message' => 'Certificado agregado correctamente a la bolsa de regalos.',
                        'redemption_id' => $redemptionID,
                        'redemptions' => $redemptions,
                        'can_redeem' => $can_redeem,
                        'certificates' => $certificates,
                    ];

                    return json_encode($response);
                }
            } else {
                $response = [
                    'status' => 'error',
                    'message' => 'El cliente <b>' . $session->get('client_fullname') . '</b> solo puede canjear <b>' . $session->get('redemptions_in_progress') . ' certificado(s)</b> en esta transacción.',
                    'current_certificates' => $session->get('current_certificates'),
                ];

                return json_encode($response);
            }
        }
    }

    public function destroy($id)
    {
        if ($this->request->isAJAX()) {
            $redemptionModel = model('RedemptionModel');
            $certificateModel = Model('CertificateModel');
            $db = \Config\Database::connect();

            $session = session();

            // Localizar el certificado a retirar de la bolsa de regalos (redención)
            $redemptionFound = $redemptionModel->find($id);

            if (isset($redemptionFound)) {

                // Ejecutar transacción de cancelación de canje (Eliminar Redención y Actualizar el Stock del Certificado seleccionado)
                $db->transBegin();

                // Eliminar la redención seleccionada
                $redemptionModel->delete($id, true);

                // Actualizar el current_stock del certificado devuelto por el cliente en +1
                // Actualizar el número de veces que este producto se ha devuelto
                // BUG ====== ModelCertificate ocasiona un error al tratar de actualizar el dato con set() en modo "escape" (FALSE)
                $builder = $db->table('certificates');
                $certificateUpdated = $builder->set('current_stock', 'current_stock + 1', false)
                                              ->set('exchanges', 'exchanges - 1', false)
                                              ->set('updated_at', 'NOW()', FALSE)
                                              ->where('id', $redemptionFound['certificate_id'])
                                              ->update();

                // Verificar si la transacción de devolución de canje fué exitosa
                if ($db->transStatus() === FALSE) {

                    // Deshacer los cambios efectuados en las tablas redemptions y certificates
                    $db->transRollback();
                    $response = [
                        'status' => 'error',
                        'message' => 'Error del sistema: No fue posible realizar la transacción de devolución de certificados. Intente nuevamente.',
                        'current_certificates' => $session->get('current_certificates'),
                    ];

                    return json_encode($response);

                } else {
                    $db->transCommit();

                    // Actualizar la sessión retirando el ID de la redención eliminada
                    $key = array_search($id, $session->get('redemptions'));

                    if ($key !== false) {
                        // Recuperar el número de redenciones (canjes) realizados en el día
                        $current_redemptions = $session->get('redemptions');

                        // Eliminar el ID de la redención registrada en sessión
                        unset($current_redemptions[$key]);
                        $session->set('redemptions', $current_redemptions);

                        // Actualizar en sessión el número de redenciones actualmente seleccionadas
                        $current_certificates = $session->get('current_certificates') - 1;
                        $session->set('current_certificates', $current_certificates);

                        // IMPORTANTE ==== FIX Error punto flotante RESTA PHP CUANDO ES CERO,
                        // Como los datos operados en los certificados actuales son de tipo entero, no ocasiona ese error

                    }

                    // Verificar si el usuario puede proceder a canjear sus certificados (Activar botón Canjear)
                    // Los certificados seleccionados sean iguales al número de redenciones permitidas en esta transacción
                    $can_redeem = $session->get('current_certificates') == $session->get('redemptions_in_progress');
                    $session->set('can_redeem', $can_redeem);

                    // Localizar todos los certificados que tengan stock en el sistema
                    // Al devolver un certificado, mi stock suma.
                    // Un cliente puede devolver un certificado cuyas existencias actuales en el sistema sean CERO
                    $certificates = $certificateModel->where('current_stock >', 0)->get()->getResultArray();

                    // Recuperar el listado de redenciones (certificados) seleccionadas actualmente por el usuario
                    $redemptions = [];
                    if(count($session->get('redemptions'))) {
                        $redemptions = $redemptionModel
                                    ->select('redemptions.id, redemptions.quantity, redemptions.created_at, users.first_name, users.last_name, certificates.name')
                                    ->join('users', 'redemptions.user_id = users.id')
                                    ->join('certificates', 'redemptions.certificate_id = certificates.id')
                                    ->whereIn('redemptions.id',  $session->get('redemptions'))
                                    ->get()
                                    ->getResultArray();
                    }

                    $response = [
                        'status' => 'success',
                        'message' => 'Certificado retirado correctamente de la bolsa de regalos.',
                        'redemption_id' => $redemptionFound,
                        'redemptions' => $redemptions,
                        'can_redeem' => $can_redeem,
                        'certificates' => $certificates,
                    ];

                    return json_encode($response);
                }
            } else {
                // Este bloque se ejecuta cuando un usuario malintensionado trata de cambiar el valor del ID del Certificado
                // a través del OPTION VALUE del SELECT vía Herramientas de desarrollo.
                $response = [
                    'status' => 'error',
                    'message' => 'Error del sistema, no fue posible eliminar la redención seleccionada',
                    'redemptionFound' => $redemptionFound,
                ];
                return json_encode($response);
            }
        }

    }

}
