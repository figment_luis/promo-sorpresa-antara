<?php
namespace App\Models;

use CodeIgniter\Model;
// use App\Entities\User;

class TicketModel extends Model
{
    protected $table = 'tickets';
    protected $primaryKey = 'id';
    protected $allowedFields = ['number', 'date', 'amount', 'store_id', 'user_id', 'admin_id'];

    protected $returnType = 'array';
    // protected $returnType = User::class;
    protected $useSoftDeletes = true;
    protected $useTimestamps = true;

    // protected $validationRules = [];
    // protected $validationMessages = [];
    // protected $skipValidation = false;

    // Definir nombres de funciones callback a ejecutarse antes de insertar los datos en el modelo
    // protected $beforeInsert = ['addGroup'];
    // protected $afterInsert = ['storeUserInfo'];

    // protected $assignGroup;
    // protected $userInfo;
}
