<?php
namespace App\Models;

use CodeIgniter\Model;
// use App\Entities\User;

class VoucherModel extends Model
{
    protected $table = 'vouchers';
    protected $primaryKey = 'id';
    protected $allowedFields = ['total_amount', 'tickets', 'redemptions', 'user_id', 'admin_id'];

    protected $returnType = 'array';
    // protected $returnType = User::class;
    protected $useSoftDeletes = true;
    protected $useTimestamps = true;

    // protected $validationRules = [];
    // protected $validationMessages = [];
    // protected $skipValidation = false;

    // Definir nombres de funciones callback a ejecutarse antes de insertar los datos en el modelo
    // protected $beforeInsert = ['addGroup'];
    // protected $afterInsert = ['storeUserInfo'];

    // protected $assignGroup;
    // protected $userInfo;
}
