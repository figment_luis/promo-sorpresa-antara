<?php
namespace App\Models;

use CodeIgniter\Model;
// use App\Entities\User;

class RoleModel extends Model
{
    protected $table = 'roles';
    protected $primaryKey = 'id';
    protected $allowedFields = ['name', 'display_name'];

    protected $returnType = 'array';
    // protected $returnType = Role::class;
    protected $useSoftDeletes = true;
    protected $useTimestamps = true;

    // protected $validationRules = [];
    // protected $validationMessages = [];
    // protected $skipValidation = false;

    // Definir nombres de funciones callback a ejecutarse antes de insertar los datos en el modelo
    // protected $beforeInsert = ['addGroup'];
    // protected $afterInsert = ['storeUserInfo'];

    // protected $assignGroup;
    // protected $userInfo;
}
