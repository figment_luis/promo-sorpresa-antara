<?php
namespace App\Models;

use CodeIgniter\Model;
// use App\Entities\User;

class CertificateModel extends Model
{
    protected $table = 'certificates';
    protected $primaryKey = 'id';
    protected $allowedFields = ['current_stock', 'updated_at'];

    protected $returnType = 'array';
    // protected $returnType = User::class;
    protected $useSoftDeletes = true;
    protected $useTimestamps = true;

    protected $updatedField  = 'updated_at';

    // protected $validationRules = [];
    // protected $validationMessages = [];
    // protected $skipValidation = false;

    // Definir nombres de funciones callback a ejecutarse antes de insertar los datos en el modelo
    // protected $beforeInsert = ['addGroup'];
    // protected $afterInsert = ['storeUserInfo'];

    // protected $assignGroup;
    // protected $userInfo;
}
