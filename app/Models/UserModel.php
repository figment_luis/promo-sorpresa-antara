<?php
namespace App\Models;

use CodeIgniter\Model;
// use App\Entities\User;

class UserModel extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $allowedFields = ['first_name', 'last_name', 'email', 'phone'];

    protected $returnType = 'array';
    // protected $returnType = User::class;
    protected $useSoftDeletes = true;
    protected $useTimestamps = true;

    // protected $validationRules = [];
    // protected $validationMessages = [];
    // protected $skipValidation = false;

    // Definir nombres de funciones callback a ejecutarse antes de insertar los datos en el modelo
    // protected $beforeInsert = ['addGroup'];
    // protected $afterInsert = ['storeUserInfo'];

    // protected $assignGroup;
    // protected $userInfo;
}
