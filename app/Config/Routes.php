<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(false);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->addRedirect('/', 'login');

// Registro de usuarios
$routes->get('/users', 'UserController::create', ['as' => 'users.create']);
$routes->post('/users', 'UserController::store', ['as' => 'users.store']);
// Buscador de usuarios
$routes->get('/search', 'UserController::search', ['as' => 'users.search']);
$routes->post('/search', 'UserController::find', ['as' => 'users.find']);

// Registro de tickets
$routes->get('/tickets', 'TicketController::create', ['as' => 'tickets.create']);
$routes->post('/tickets', 'TicketController::store', ['as' => 'tickets.store']);
// Eliminar tickets
$routes->post('/tickets/(:segment)', 'TicketController::destroy/$1', ['as' => 'tickets.destroy']);

// Registro de Redenciones (Selección de Certificados por el cliente)
$routes->get('/redemptions', 'RedemptionController::create', ['as' => 'redemptions.create']);
$routes->post('/redemptions', 'RedemptionController::store', ['as' => 'redemptions.store']);
// Eliminar Redenciones
$routes->post('/redemptions/(:segment)', 'RedemptionController::destroy/$1', ['as' => 'redemptions.destroy']);

// Generar comprobante o voucher
$routes->post('/vouchers', 'VoucherController::print', ['as' => 'vourchers.print']);

// Panel de Control (Dashboard)
$routes->get('/dashboard', 'Home::dashboard', ['as' => 'dashboard']);

// Login
$routes->get('/login', 'AuthController::login', ['as' => 'auth.login']);
$routes->post('/login', 'AuthController::alogin', ['as' => 'auth.alogin']);
// Logout
$routes->post('/logout', 'AuthController::logout', ['as' => 'auth.logout']);

// Cancelar Transacción
$routes->post('/cancel-transaction', 'TransactionController::cancel', ['as' => 'transaction.cancel']);

// Términos y Condiciones
$routes->get('/terms_and_conditions', 'PageController::terms', ['as' => 'page.terms']);

// Reportes
$routes->get('/report', 'PageController::report', ['as' => 'page.report']);


/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
