<?php namespace Config;

class Validation
{
	//--------------------------------------------------------------------
	// Setup
	//--------------------------------------------------------------------

	/**
	 * Stores the classes that contain the
	 * rules that are available.
	 *
	 * @var array
	 */
	public $ruleSets = [
		\CodeIgniter\Validation\Rules::class,
		\CodeIgniter\Validation\FormatRules::class,
		\CodeIgniter\Validation\FileRules::class,
		\CodeIgniter\Validation\CreditCardRules::class,
	];

	/**
	 * Specifies the views that are used to display the
	 * errors.
	 *
	 * @var array
	 */
	public $templates = [
		'list'   => 'CodeIgniter\Validation\Views\list',
		'single' => 'CodeIgniter\Validation\Views\single',
	];

	//--------------------------------------------------------------------
	// Rules
	//--------------------------------------------------------------------

	// REGISTRO DE CLIENTES
	public $signup = [
		'first_name' => 'required|trim|min_length[3]|regex_match[/[\p{L}a-zA-Z ]+$/i]',
        'last_name' => 'required|trim|min_length[3]|regex_match[/[\p{L}a-zA-Z ]+$/i]',
        'email' => 'required|trim|valid_email|is_unique[users.email]',
        'phone' => 'required|trim|regex_match[/^[0-9]{10}$/]|is_unique[users.phone]',
	];

	public $signup_errors = [
		'first_name' => [
            'required' => 'El nombre del cliente es requierido',
            'regex_match' => 'El nombre del cliente parece ser un dato no válido',
            'min_length' => 'El nombre del cliente debe contener al menos 3 caracteres'
        ],
        'last_name' => [
            'required' => 'Los apellidos del cliente son requeridos',
            'regex_match' => 'Los apellidos del cliente parecen ser datos no válidos',
            'min_length' => 'Los apellidos del cliente deben contener al menos 3 caracteres'
        ],
        'email' => [
            'required' => 'La dirección de correo electrónico es requerida',
            'valid_email' => 'La dirección de correo electrónico parece ser un dato no válido',
            'is_unique' => 'La dirección de correo electrónico ya está en uso por otro cliente'
        ],
        'phone' => [
            'required' => 'El número telefónico es requerido',
            'regex_match' => 'El formato de número telefónico es incorrecto (10 dígitos)',
            'is_unique' => 'El número telefónico ya está en uso por otro cliente'
        ]
	];

	// BUSCAR CLIENTES POR EMAIL
	public $email = [
		'query' => 'required|trim|valid_email'
	];
	public $email_errors = [
		'query' => [
			'required' => 'La dirección de correo electrónico es requerida',
			'valid_email' => 'La dirección de correo electrónico parece ser un dato no válido',
		]
	];

	// BUSCAR CLIENTES POR TELÉFONO
	public $phone = [
		'query' => 'required|trim|regex_match[/^[0-9]{10}$/]'
	];
	public $phone_errors = [
		'query' => [
			'required' => 'El número telefónico es requerido',
			'regex_match' => 'El formato de número telefónico es incorrecto (10 dígitos)',
		]
	];

    // LOGIN
    public $login = [
        'email' => 'required|trim|valid_email',
        'password' => 'required',
    ];
    public $login_errors = [
        'email' => [
            'required' => 'La dirección de correo electrónico es requerida',
            'valid_email' => 'La dirección de correo electrónico parece ser un dato no válido',
        ],
        'password' => [
            'required' => 'La contraseña es requerida',
        ]
    ];
}
