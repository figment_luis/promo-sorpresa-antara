<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class StoresTableSeeder extends Seeder
{
	public function run()
	{
		// Sembrar tiendas participantes en el sistema
        $stores = [
            [
                'name' => 'Zara Home',
                'created_at' => '2020-12-03 12:00:00'
            ],
            [
                'name' => "Victoria´s Secret",
                'created_at' => '2020-12-03 12:00:00'
            ]
        ];

        $builder = $this->db->table('stores');
        $builder->insertBatch($stores);
	}
}
