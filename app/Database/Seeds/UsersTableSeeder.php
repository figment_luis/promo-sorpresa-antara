<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class UsersTableSeeder extends Seeder
{
	public function run()
	{
		// Sembrar administradores/clientes disponibles en el sistema
        $users = [
            [
                'first_name' => 'Ossiel',
                'last_name' => 'Flores Avila',
                'email' => 'ossiel@kinetiq.com.mx',
                'phone' => '7222618738',
                'password' => password_hash('123456789', PASSWORD_BCRYPT),
                'role_id' => 1,
                'created_at' => '2020-12-09 12:00:00'
            ]
        ];

        $builder = $this->db->table('users');
        $builder->insertBatch($users);
	}
}
