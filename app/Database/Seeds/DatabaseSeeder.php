<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	public function run()
	{
		// Sembrar datos falsos en base de datos
        $this->call('RolesTableSeeder');
        $this->call('UsersTableSeeder');
        // $this->call('StoresTableSeeder');
        $this->call('CertificatesTableSeeder');
	}
}
