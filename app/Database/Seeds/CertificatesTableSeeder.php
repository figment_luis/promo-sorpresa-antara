<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class CertificatesTableSeeder extends Seeder
{
	public function run()
	{
		// Sembrar certificados disponibles en el sistema
        $certificates = [
            [
                'name' => 'Certificado de $1,000 - Abercrombie & Fitch',
                'description' => 'Abercrombie & Fitch',
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => 'Certificado de $1,000 - Aeromexico',
                'description' => 'Aeromexico',
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => 'Certificado de $1,000 - Aldo',
                'description' => 'Aldo',
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => 'Certificado de $1,000 - All Saints',
                'description' => 'All Saints',
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => 'Certificado de $1,000 - American Eagle Outfitters',
                'description' => 'American Eagle Outfitters',
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => 'Certificado de $1,000 - Bershka',
                'description' => 'Bershka',
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => 'Certificado de $1,000 - Bobbi Brown',
                'description' => 'Bobbi Brown',
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => 'Certificado de $1,000 - Caudalie',
                'description' => 'Caudalie',
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => 'Certificado de $1,000 - Coach',
                'description' => 'Coach',
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => 'Certificado de $1,000 - Desigual',
                'description' => 'Desigual',
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ], //
            [
                'name' => 'Certificado de $1,000 - EA7',
                'description' => 'EA7',
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => 'Certificado de $1,000 - El Ganso',
                'description' => 'El Ganso',
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => 'Certificado de $1,000 - Gran Vía',
                'description' => 'Gran Vía',
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => 'Certificado de $1,000 - Hackett',
                'description' => 'Hackett',
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => 'Certificado de $1,000 - Hamleys',
                'description' => 'Hamleys',
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => 'Certificado de $1,000 - Hugo RED',
                'description' => 'Hugo RED',
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => 'Certificado de $1,000 - Hugo Boss',
                'description' => 'Hugo Boss',
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => 'Certificado de $1,000 - Jo Malone',
                'description' => 'Jo Malone',
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => 'Certificado de $1,000 - Julio',
                'description' => 'Julio',
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => 'Certificado de $1,000 - Kate Spade',
                'description' => 'Kate Spade',
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => "Certificado de $1,000 - Kiehl's",
                'description' => "Kiehl's",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ], //
            [
                'name' => "Certificado de $1,000 - Kipling",
                'description' => "Kipling",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => "Certificado de $1,000 - La Cabrera",
                'description' => "La Cabrera",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => "Certificado de $1,000 - La No. 20",
                'description' => "La No. 20",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => "Certificado de $1,000 - Levis",
                'description' => "Levis",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => "Certificado de $1,000 - Mango",
                'description' => "Mango",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => "Certificado de $1,000 - Marella",
                'description' => "Marella",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => "Certificado de $1,000 - Massimo Dutti",
                'description' => "Massimo Dutti",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => "Certificado de $1,000 - Moshi Moshi",
                'description' => "Moshi Moshi",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => "Certificado de $1,000 - Oysho",
                'description' => "Oysho",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => "Certificado de $1,000 - Pull & Bear",
                'description' => "Pull & Bear",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ], //
            [
                'name' => "Certificado de $1,000 - Puma",
                'description' => "Puma",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => "Certificado de $1,000 - Sandro",
                'description' => "Sandro",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => "Certificado de $1,000 - Scalpers",
                'description' => "Scalpers",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => "Certificado de $1,000 - Sephora",
                'description' => "Sephora",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => "Certificado de $1,000 - Steve Madden",
                'description' => "Steve Madden",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => "Certificado de $1,000 - Tommy Hilfiger",
                'description' => "Tommy Hilfiger",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => "Certificado de $1,000 - Tommy Jeans",
                'description' => "Tommy Jeans",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => "Certificado de $1,000 - UGG Australia",
                'description' => "UGG Australia",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => "Certificado de $1,000 - Uterqüe",
                'description' => "Uterqüe",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => "Certificado de $1,000 - Zara",
                'description' => "Zara",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ], //
            [
                'name' => "Certificado de $1,000 - Zara Home",
                'description' => "Zara Home",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => "Certificado de $1,000 - Zingara",
                'description' => "Zingara",
                'initial_stock' => 10,
                'current_stock' => 10,
                'image' => NULL,
                'exchanges' => NULL,
                'created_at' => '2020-12-09 12:00:00'
            ],
        ];

        $builder = $this->db->table('certificates');
        $builder->insertBatch($certificates);
	}
}
