<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class RolesTableSeeder extends Seeder
{
	public function run()
	{
		// Sembrar roles disponibles en el sistema (admin, client)
        $roles = [
            [
                'name' => 'admin',
                'display_name' => 'Administrador',
                'created_at' => '2020-12-09 12:00:00'
            ],
            [
                'name' => 'client',
                'display_name' => 'Cliente',
                'created_at' => '2020-12-09 12:00:00'
            ]
        ];

        $builder = $this->db->table('roles');
        $builder->insertBatch($roles);
	}
}
