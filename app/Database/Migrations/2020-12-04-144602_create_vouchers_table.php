<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateVouchersTable extends Migration
{
	public function up()
	{
		// Generar estructura de tabla
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'auto_increment' => true,
                'unsigned' => true,
            ],
            'total_amount' => [
                'type' => 'DECIMAL',
                'constraint' => '10,2',
                'unsigned' => true,
            ],
            'tickets' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'redemptions' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'user_id' => [
                'type' => 'INT',
                'unsigned' => true,
                'null' => true,
            ],
            'admin_id' => [
                'type' => 'INT',
                'unsigned' => true,
                'null' => true,
            ],
            'created_at' => [
                'type' => 'DATETIME'
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'deleted_at' => [
                'type' => 'DATETIME',
                'null' => true
            ],
        ]);
        // Generar llave primaria
        $this->forge->addPrimaryKey('id');
        // Generar llaves foráneas
        $this->forge->addForeignKey('user_id', 'users', 'id', 'CASCADE', 'SET NULL');
        // Crear tabla en base de datos
        $this->forge->createTable('vouchers');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		// Eliminar tabla de base de datos
        $this->forge->dropTable('vouchers');
	}
}
