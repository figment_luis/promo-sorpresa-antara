<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateCertificatesTable extends Migration
{
	public function up()
	{
		// Generar estructura de tabla
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'auto_increment' => true,
                'unsigned' => true,
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'description' => [
                'type' => 'TEXT',
            ],
            'initial_stock' => [
                'type' => 'INT',
                'unsigned' => true,
            ],
            'current_stock' => [
                'type' => 'INT',
                'unsigned' => true,
            ],
            'image' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true,
            ],
            'exchanges' => [
                'type' => 'INT',
                'unsigned' => true,
                'default' => 0,
            ],
            'created_at' => [
                'type' => 'DATETIME'
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'deleted_at' => [
                'type' => 'DATETIME',
                'null' => true
            ],
        ]);
        // Generar llave primaria
        $this->forge->addPrimaryKey('id');
        // Crear tabla en base de datos
        $this->forge->createTable('certificates');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		// Eliminar tabla de base de datos
        $this->forge->dropTable('certificates');
	}
}
