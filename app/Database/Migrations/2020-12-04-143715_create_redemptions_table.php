<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateRedemptionsTable extends Migration
{
	public function up()
	{
		// Generar estructura de tabla
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'auto_increment' => true,
                'unsigned' => true,
            ],
            'quantity' => [
                'type' => 'INT',
                'unsigned' => true,
            ],
            'certificate_id' => [
                'type' => 'INT',
                'unsigned' => true,
                'null' => true,
            ],
            'user_id' => [
                'type' => 'INT',
                'unsigned' => true,
                'null' => true,
            ],
            'admin_id' => [
                'type' => 'INT',
                'unsigned' => true,
                'null' => true,
            ],
            'created_at' => [
                'type' => 'DATETIME'
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'deleted_at' => [
                'type' => 'DATETIME',
                'null' => true
            ],
        ]);
        // Generar llave primaria
        $this->forge->addPrimaryKey('id');
        // Generar llaves foráneas
        $this->forge->addForeignKey('certificate_id', 'certificates', 'id', 'CASCADE', 'SET NULL');
        $this->forge->addForeignKey('user_id', 'users', 'id', 'CASCADE', 'SET NULL');
        // Crear tabla en base de datos
        $this->forge->createTable('redemptions');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		// Eliminar tabla de base de datos
        $this->forge->dropTable('redemptions');
	}
}
