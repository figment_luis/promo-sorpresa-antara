<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateRolesTable extends Migration
{
	public function up()
	{
		// Generar estructura de tabla
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'auto_increment' => true,
                'unsigned' => true,
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 10,
                'unique' => true,
            ],
            'display_name' => [
                'type' => 'VARCHAR',
                'constraint' => 20,
                'unique' => true,
            ],
            'created_at' => [
                'type' => 'DATETIME'
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => true
            ],
            'deleted_at' => [
                'type' => 'DATETIME',
                'null' => true
            ],
        ]);
        // Generar llave primaria
        $this->forge->addPrimaryKey('id');
        // Crear tabla en base de datos
        $this->forge->createTable('roles');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		// Eliminar tabla de base de datos
        $this->forge->dropTable('roles');
	}
}
